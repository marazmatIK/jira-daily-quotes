package com.perfectcommit.jira.dq.services;

import com.perfectcommit.jira.dq.ao.dao.UserConfigDao;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.domain.Quote;
import com.perfectcommit.jira.dq.mocks.QuoteMockDao;
import com.perfectcommit.jira.dq.mocks.UserConfigMockDao;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by zugzug on 21.07.2017.
 */
public class QuoteManagerTest {
    private static final UserConfigDao userConfigDao = new UserConfigMockDao();
    private static final QuoteMockDao quoteMockDao = new QuoteMockDao();

    static {
        quoteMockDao.addQuote(new Quote(Category.BUSINESS, "N/A", "N/A", "Though", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.HUMOR, "N/A2", "N/A2", "Though2", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.SELF_IMPROVEMENT, "N/A3", "N/A3", "Though3", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.INSPIRATION, "N/A4", "N/A4", "Though4", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.MANAGEMENT, "N/A5", "N/A5", "Though5", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A6", "N/A6", "Though6", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A7", "N/A7", "Though7", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A8", "N/A8", "Though8", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A9", "N/A9", "Though9", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A10", "N/A10", "Though10", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A11", "N/A11", "Though11", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A12", "N/A12", "Though12", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A13", "N/A13", "Though13", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A14", "N/A14", "Though14", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A15", "N/A15", "Though15", new ArrayList<String>()));
        quoteMockDao.addQuote(new Quote(Category.QA, "N/A16", "N/A16", "Though16", new ArrayList<String>()));
    }

    @Test
    public void settingsTest() throws InterruptedException {
        QuotesManager quotesManager = new QuotesManager(quoteMockDao, userConfigDao, new UserActionsHistoryService());
        quotesManager.tickSize = 1;
        userConfigDao.saveConfig("admin", 1);

        Quote quote = quotesManager.quoteForUser("admin");
        Thread.sleep((quotesManager.tickSize + 2) * 1000L);
        Quote quote2 = quotesManager.quoteForUser("admin");

        assertNotEquals(quote.getText(), quote2.getText());
    }

    @Test
    public void sameQuoteInPeriod() throws InterruptedException {
        QuotesManager quotesManager = new QuotesManager(quoteMockDao, userConfigDao, new UserActionsHistoryService());
        quotesManager.tickSize = 6;
        userConfigDao.saveConfig("admin", 1);

        Quote quote = quotesManager.quoteForUser("admin");
        Thread.sleep(2 * 1000L);
        Quote quote2 = quotesManager.quoteForUser("admin");
        assertEquals(quote.getText(), quote2.getText());
        Thread.sleep(2 * 1000L);
        Quote quote3 = quotesManager.quoteForUser("admin");
        assertEquals(quote.getText(), quote3.getText());
        assertEquals(quote2.getText(), quote3.getText());
    }

    @Test
    public void differentUsers() throws InterruptedException {
        QuotesManager quotesManager = new QuotesManager(quoteMockDao, userConfigDao, new UserActionsHistoryService());
        quotesManager.tickSize = 1;
        userConfigDao.saveConfig("admin", 1);

        userConfigDao.saveConfig("developer", 5);

        Quote quote11 = quotesManager.quoteForUser("admin");
        Quote quote21 = quotesManager.quoteForUser("developer");
        Thread.sleep(2 * 1000L);
        Quote quote12 = quotesManager.quoteForUser("admin");
        Quote quote22 = quotesManager.quoteForUser("developer");

        assertNotEquals(quote11.getText(), quote12.getText());
        assertEquals(quote21.getText(), quote22.getText());

        Thread.sleep(5 * 1000L);

        Quote quote13 = quotesManager.quoteForUser("admin");
        Quote quote23 = quotesManager.quoteForUser("developer");

        assertNotEquals(quote13.getText(), quote12.getText());
        assertNotEquals(quote22.getText(), quote23.getText());
    }
}
