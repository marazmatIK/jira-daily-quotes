package com.perfectcommit.jira.dq.mocks;

import com.perfectcommit.jira.dq.ao.dao.UserConfigDao;
import com.perfectcommit.jira.dq.ao.dao.model.UserConfig;
import com.perfectcommit.jira.dq.domain.Category;
import net.java.ao.EntityManager;
import net.java.ao.RawEntity;

import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zugzug on 21.07.2017.
 */
public class UserConfigMockDao implements UserConfigDao {
    private static class MockUserConfig implements UserConfig {
        private String username;
        private int quotePeriod;

        public MockUserConfig(String username, int quotePeriod) {
            this.username = username;
            this.quotePeriod = quotePeriod;
        }

        @Override
        public String getUsername() {
            return username;
        }

        @Override
        public int getQuotePeriod() {
            return quotePeriod;
        }

        @Override
        public String getAuthor() {
            return null;
        }

        @Override
        public Category getCategory() {
            return null;
        }

        @Override
        public void setQuotePeriod(int quotePeriod) {

        }

        @Override
        public void setAuthor(String author) {

        }

        @Override
        public void setCategory(Category category) {

        }

        @Override
        public int getID() {
            return 0;
        }

        @Override
        public void init() {

        }

        @Override
        public void save() {

        }

        @Override
        public EntityManager getEntityManager() {
            return null;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {

        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {

        }

        @Override
        public <X extends RawEntity<Integer>> Class<X> getEntityType() {
            return null;
        }
    }

    private Map<String, UserConfig> userConfigMap = new HashMap<>();

    @Override
    public UserConfig saveConfig(String username, int quotePeriod, String author, Category category) {
        UserConfig userConfig = new MockUserConfig(username, quotePeriod);
        userConfigMap.put(username, userConfig);
        return userConfig;
    }

    @Override
    public UserConfig saveConfig(String username, int quotePeriod) {
        UserConfig userConfig = new MockUserConfig(username, quotePeriod);
        userConfigMap.put(username, userConfig);
        return userConfig;
    }

    @Override
    public UserConfig getConfig(String username) {
        return userConfigMap.get(username);
    }
}
