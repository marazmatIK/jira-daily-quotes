package com.perfectcommit.jira.dq.mocks;

import com.perfectcommit.jira.dq.ao.dao.QuoteDao;
import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.domain.Quote;
import net.java.ao.EntityManager;
import net.java.ao.RawEntity;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zugzug on 21.07.2017.
 */
public class QuoteMockDao implements QuoteDao {

    private static class MockAoQuote implements AoQuote {
        private String text, authorName, authorPosition, authorUrl, keyWords;
        private Category category;

        public MockAoQuote(String text, String authorName, String authorPosition, String authorUrl, String keyWords, Category category) {
            this.text = text;
            this.authorName = authorName;
            this.authorPosition = authorPosition;
            this.authorUrl = authorUrl;
            this.keyWords = keyWords;
            this.category = category;
        }

        @Override
        public String getAuthorName() {
            return authorName;
        }

        @Override
        public void setAuthorName(String authorName) {
            this.authorName = authorName;
        }

        @Override
        public String getAuthorPosition() {
            return authorPosition;
        }

        @Override
        public void setAuthorPosition(String authorPosition) {
            this.authorPosition = authorPosition;
        }

        @Override
        public String getQuoteText() {
            return text;
        }

        @Override
        public void setQuoteText(String quoteText) {
            this.text = quoteText;
        }

        @Override
        public String getAuthorUrl() {
            return authorUrl;
        }

        @Override
        public void setAuthorUrl(String authorUrl) {
            this.authorUrl = authorUrl;
        }

        @Override
        public String getKeyWords() {
            return keyWords;
        }

        @Override
        public void setKeyWords(String keyWords) {
            this.keyWords = keyWords;
        }

        @Override
        public Category getCategory() {
            return category;
        }

        @Override
        public void setCategory(Category category) {
            this.category = category;
        }

        @Override
        public int getID() {
            return 0;
        }

        @Override
        public void init() {

        }

        @Override
        public void save() {

        }

        @Override
        public EntityManager getEntityManager() {
            return null;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {

        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {

        }

        @Override
        public <X extends RawEntity<Integer>> Class<X> getEntityType() {
            return null;
        }
    }

    List<AoQuote> quotes = new LinkedList<>();

    @Override
    public AoQuote get(int position) {
        return quotes.get(position);
    }

    @Override
    public List<AoQuote> find(String authorName) {
        List<AoQuote> res = new LinkedList<>();
        for(AoQuote quote: quotes) {
            if (authorName.equals(quote.getAuthorName())) {
                res.add(quote);
            }
        }
        return res;
    }

    @Override
    public List<AoQuote> find(Category category) {
        List<AoQuote> res = new LinkedList<>();
        for(AoQuote quote: quotes) {
            if (category == quote.getCategory()) {
                res.add(quote);
            }
        }
        return res;
    }

    @Override
    public List<AoQuote> findAll() {
        return new ArrayList<>(quotes);
    }

    @Override
    public int quotesCount() {
        return quotes.size();
    }

    @Override
    public AoQuote getById(String quoteId) {
        return null;
    }

    @Override
    public void delete(String quoteId) {

    }

    //TODO: include in API?
    public AoQuote addQuote(Quote quote) {
        AoQuote aoQuote = new MockAoQuote(quote.getText(), quote.getAuthorName(), quote.getAuthorPosition(), quote.getAuthorUrl(), quote.getKeyWords().toString(), quote.getCategory());
        quotes.add(aoQuote);
        return aoQuote;
    }
}
