package com.perfectcommit.jira.dq;

import com.perfectcommit.jira.dq.domain.Quote;
import com.perfectcommit.jira.dq.services.QuotesManager;
import org.junit.Test;

import javax.xml.bind.JAXBException;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by zugzug on 20.07.2017.
 */
public class MarshallerTest {

    private static final QuotesManager quotesManager = new QuotesManager(null,null, null);
    @Test
    public void testQuotesMarshaller() throws JAXBException, IOException {
        List<Quote> quotes = quotesManager.loadFromResources("volume1.xml");
        assertEquals(74, quotes.size());
        System.out.println(quotes.get(0).getKeyWords());
        System.out.println(quotes.get(0).getKeyWords().size());
        System.out.println(quotes.get(71).getText());
    }
}
