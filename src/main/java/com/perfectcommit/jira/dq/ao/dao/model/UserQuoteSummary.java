package com.perfectcommit.jira.dq.ao.dao.model;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * Created by nchernov on 21-Jul-16.
 */
@Preload
@Table("QuoteSummary")
public interface UserQuoteSummary extends Entity {
    String getUserName();
    long getLastDateViewed();
    int getQuoteId();
    int getHitCount();
}
