package com.perfectcommit.jira.dq.ao.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.google.common.collect.ImmutableMap;
import com.perfectcommit.jira.dq.ao.dao.model.UserConfig;
import com.perfectcommit.jira.dq.domain.Category;
import net.java.ao.Query;
/**
 * Created by zugzug on 21.07.2017.
 */
public class UserConfigDaoImpl implements UserConfigDao {

    private final ActiveObjects ao;

    public UserConfigDaoImpl(ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    public UserConfig saveConfig(String username, int quotePeriod, String author, Category category) {
        UserConfig[] userConfig = ao.find(UserConfig.class, Query.select().where("USERNAME = ?", username));
        UserConfig userConfigItem = null;
        if (userConfig == null || userConfig.length  == 0) {
            ImmutableMap.Builder<String, Object> parameters = ImmutableMap.<String, Object>builder()
                    .put("USERNAME", username)
                    .put("QUOTE_PERIOD", quotePeriod)
                    .put("AUTHOR", author == null ? "" : author)
                    .put("CATEGORY", category);
            userConfigItem = ao.create(UserConfig.class, parameters.build());
        } else {
            userConfigItem = userConfig[0];
            userConfigItem.setQuotePeriod(quotePeriod);
            userConfigItem.setAuthor(author);
            userConfigItem.setCategory(category);
            userConfigItem.save();
        }
        return userConfigItem;
    }

    @Override
    public UserConfig saveConfig(String username, int quotePeriod) {
        UserConfig[] userConfig = ao.find(UserConfig.class, Query.select().where("USERNAME = ?", username));
        UserConfig userConfigItem = null;
        if (userConfig == null || userConfig.length  == 0) {
            ImmutableMap.Builder<String, Object> parameters = ImmutableMap.<String, Object>builder()
                    .put("USERNAME", username)
                    .put("QUOTE_PERIOD", quotePeriod);
            userConfigItem = ao.create(UserConfig.class, parameters.build());
        } else {
            userConfigItem = userConfig[0];
            userConfigItem.setQuotePeriod(quotePeriod);
            userConfigItem.save();
        }
        return userConfigItem;
    }

    @Override
    public UserConfig getConfig(String username) {
        UserConfig[] userConfig = ao.find(UserConfig.class, Query.select().where("USERNAME = ?", username));
        return (userConfig == null || userConfig.length  == 0) ? null : userConfig[0];
    }
}
