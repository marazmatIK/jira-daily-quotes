package com.perfectcommit.jira.dq.ao.dao.model;

import com.perfectcommit.jira.dq.domain.Category;
import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * Created by zugzug on 21.07.2017.
 */
@Preload
@Table("UserConfig")
public interface UserConfig extends Entity {
    String getUsername();
    int getQuotePeriod();

    String getAuthor();
    Category getCategory();

    void setQuotePeriod(int quotePeriod);
    void setAuthor(String author);
    void setCategory(Category category);
}
