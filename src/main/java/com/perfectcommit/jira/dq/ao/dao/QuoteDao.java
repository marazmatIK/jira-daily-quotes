package com.perfectcommit.jira.dq.ao.dao;

import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.domain.Quote;

import java.util.List;

/**
 * Created by zugzug on 19.07.2017.
 */
public interface QuoteDao {

    AoQuote get(int position);

    List<AoQuote> find(String authorName);

    List<AoQuote> find(Category category);

    List<AoQuote> findAll();

    AoQuote addQuote(Quote quote);

    int quotesCount();

    AoQuote getById(String quoteId);

    void delete(String quoteId);
}
