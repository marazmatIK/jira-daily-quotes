package com.perfectcommit.jira.dq.ao.dao.model;

import com.perfectcommit.jira.dq.domain.Category;
import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

/**
 * Created by nchernov on 21-Jul-16.
 */
@Preload
@Table("Quote")
public interface AoQuote extends Entity {

    Category getCategory();
    void setCategory(Category category);
    String getAuthorName();
    void setAuthorName(String authorName);
    String getAuthorPosition();
    void setAuthorPosition(String authorPosition);
    String getQuoteText();
    @StringLength(StringLength.UNLIMITED)
    void setQuoteText(String quoteText);
    String getKeyWords();
    @StringLength(StringLength.UNLIMITED)
    void setKeyWords(String keyWords);
    String getAuthorUrl();
    void setAuthorUrl(String authorUrl);
}
