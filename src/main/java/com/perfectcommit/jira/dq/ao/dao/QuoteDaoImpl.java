package com.perfectcommit.jira.dq.ao.dao;

import java.util.Arrays;
import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.google.common.collect.ImmutableMap;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import com.perfectcommit.jira.dq.domain.Quote;
import net.java.ao.Query;
import org.apache.commons.lang.StringUtils;

/**
 * Created by zugzug on 09.08.16.
 */
public class QuoteDaoImpl implements QuoteDao {
    private final ActiveObjects ao;

    public QuoteDaoImpl(ActiveObjects ao) {
        this.ao = ao;
    }

    public AoQuote get(int position) {
        Query query = Query.select().limit(1).offset(position);
        AoQuote[] result = ao.find(AoQuote.class, query);
        return result == null ? null : result.length < 1 ? null : result[0];
    }

    public List<AoQuote> find(String authorName) {
        Query query = Query.select().where("AUTHOR_NAME = ?", authorName);
        AoQuote[] result = ao.find(AoQuote.class, query);
        return Arrays.asList(result);
    }

    public List<AoQuote> find(Category category) {
        Query query = Query.select().where("CATEGORY = ?", category.name());
        AoQuote[] result = ao.find(AoQuote.class, query);
        return Arrays.asList(result);
    }

    public List<AoQuote> findAll() {
        return Arrays.asList(ao.find(AoQuote.class));
    }

    @Override
    public AoQuote addQuote(Quote quote) {
        ImmutableMap.Builder<String, Object> parameters = ImmutableMap.<String, Object>builder()
                .put("AUTHOR_NAME", quote.getAuthorName())
                .put("QUOTE_TEXT", quote.getText())
                .put("AUTHOR_POSITION", quote.getAuthorPosition() == null ? "" : quote.getAuthorPosition())
                .put("AUTHOR_URL", quote.getAuthorUrl() == null ? "" : quote.getAuthorUrl() )
                .put("CATEGORY", quote.getCategory())
                .put("KEY_WORDS", quote.getKeyWords() == null ? "" :
                        quote.getKeyWords().isEmpty() ? "" : StringUtils.join(quote.getKeyWords(), ","));
        AoQuote aoQuote = ao.create(AoQuote.class, parameters.build());
        return aoQuote;
    }

    public int quotesCount() {
        return ao.count(AoQuote.class);
    }

    @Override
    public AoQuote getById(String quoteId) {
        Query query = Query.select().limit(1).where("ID = ?", quoteId);
        AoQuote[] result = ao.find(AoQuote.class, query);
        return result == null ? null : result.length < 1 ? null : result[0];
    }

    @Override
    public void delete(String quoteId) {
        AoQuote aoQuote = getById(quoteId);
        if (aoQuote != null) {
            ao.delete(aoQuote);
        }
    }
}
