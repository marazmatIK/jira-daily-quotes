package com.perfectcommit.jira.dq.ao.dao;

import com.perfectcommit.jira.dq.ao.dao.model.UserConfig;
import com.perfectcommit.jira.dq.domain.Category;

/**
 * Created by zugzug on 21.07.2017.
 */
public interface UserConfigDao {
    UserConfig saveConfig(String username, int quotePeriod, String author, Category category);
    UserConfig saveConfig(String username, int quotePeriod);
    UserConfig getConfig(String username);
}
