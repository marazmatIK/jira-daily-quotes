package com.perfectcommit.jira.dq.ao.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.google.common.collect.ImmutableMap;
import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import com.perfectcommit.jira.dq.domain.Quote;
import com.perfectcommit.jira.dq.services.QuotesManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by zugzug on 19.07.2017.
 */
public class QuotesUpgradeV1 implements ActiveObjectsUpgradeTask {
    private static final Logger _log = Logger.getLogger(QuotesUpgradeV1.class);
    private final QuotesManager quotesManager;

    public QuotesUpgradeV1(QuotesManager quotesManager) {
        this.quotesManager = quotesManager;
    }

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("1");
    }

    @Override
    public void upgrade(ModelVersion current, ActiveObjects ao) {
        _log.info("Started migration patch. Current version in database is " + current.toString());

        long count = 0;
        ao.migrate(AoQuote.class);
        try {
            List<Quote> quoteList = quotesManager.loadFromResources("volume1.xml");
            for(Quote quote: quoteList) {
                ImmutableMap.Builder<String, Object> parameters = ImmutableMap.<String, Object>builder()
                        .put("AUTHOR_NAME", quote.getAuthorName())
                        .put("QUOTE_TEXT", quote.getText())
                        .put("AUTHOR_POSITION", quote.getAuthorPosition() == null ? "" : quote.getAuthorPosition())
                        .put("AUTHOR_URL", quote.getAuthorUrl() == null ? "" : quote.getAuthorUrl() )
                        .put("CATEGORY", quote.getCategory())
                        .put("KEY_WORDS", quote.getKeyWords() == null ? "" :
                                quote.getKeyWords().isEmpty() ? "" : StringUtils.join(quote.getKeyWords(), ","));
                ao.create(AoQuote.class, parameters.build());
                count++;
            }
            _log.info("Migration finished. Successful converted resource entities to db records: " + count);
        } catch (JAXBException e) {
            _log.error("Failed to perform migration: " + e.getMessage(), e);
        }

    }
}
