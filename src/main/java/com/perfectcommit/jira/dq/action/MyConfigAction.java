package com.perfectcommit.jira.dq.action;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.perfectcommit.jira.dq.ao.dao.UserConfigDao;
import com.perfectcommit.jira.dq.ao.dao.model.UserConfig;
import webwork.action.ActionContext;

/**
 * Created by zugzug on 21.07.2017.
 */
public class MyConfigAction extends BaseAction {
    private final boolean isPost;
    private int quotePeriod = 60;
    private UserConfigDao userConfigDao;
    private I18nResolver i18nResolver;

    public MyConfigAction(JiraAuthenticationContext jiraAuthenticationContext, UserConfigDao userConfigDao, I18nResolver i18nResolver, PermissionManager permissionManager) {
        super(jiraAuthenticationContext, permissionManager);
        this.userConfigDao = userConfigDao;
        this.i18nResolver = i18nResolver;
        this.isPost = "POST".equalsIgnoreCase(ActionContext.getRequest().getMethod());
        UserConfig userConfig = userConfigDao.getConfig(getJiraUser());
        if (userConfig != null) {
            quotePeriod = userConfig.getQuotePeriod();
        }
    }

    public int getQuotePeriod() {
        return quotePeriod;
    }

    public void setQuotePeriod(int quotePeriod) {
        this.quotePeriod = quotePeriod;
    }

    @Override
    protected void doValidation() {
        super.doValidation();
        if (getHasErrors()) {
            return;
        }
        if (isPost) {
            if (quotePeriod < 0) {
                addError("account", i18nResolver.getText("dc.create.user.config.error.invalid.period"));
                return;
            }
        }
    }

    @Override
    public String doDefault() {
        return SUCCESS;
    }

    @Override
    protected String doExecute() throws Exception {
        if (isPost) {
            userConfigDao.saveConfig(getJiraUser(), quotePeriod);
        }
        return SUCCESS;
    }
}
