package com.perfectcommit.jira.dq.action;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.perfectcommit.jira.dq.ao.dao.QuoteDao;
import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.domain.Quote;
import webwork.action.ActionContext;
import webwork.action.ServletActionContext;

import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Created by zugzug on 23.07.2017.
 */
public class QuoteOperationAction extends BaseAction {
    private final boolean isPost;

    private final QuoteDao quoteDao;
    private String quoteText = "Quote here..";
    private String author = "";
    private String category = "";
    private String quoteId = "";

    public QuoteOperationAction(JiraAuthenticationContext jiraAuthenticationContext, QuoteDao quoteDao, PermissionManager permissionManager) {
        super(jiraAuthenticationContext, permissionManager);
        this.quoteDao = quoteDao;
        this.isPost = "POST".equalsIgnoreCase(ActionContext.getRequest().getMethod());
        quoteId = ServletActionContext.getRequest().getParameter("id");
        AoQuote aoQuote = quoteDao.getById(quoteId);
        if (aoQuote != null) {
            quoteText = aoQuote.getQuoteText();
            author = aoQuote.getAuthorName();
            category = aoQuote.getCategory().name();
        }
    }

    public List<String> getCategories() {
        List<String> catValues = new LinkedList<>();
        for(Category category: Category.values()) {
            catValues.add(category.name());
        }
        return catValues;
    }

    @Override
    protected void doValidation() {
        super.doValidation();
        if (getHasErrors()) {
            return;
        }
        if(!hasAdminAccess(getCurrentUser())) {
            addError("account", "Only admins have access to this page");
            return;
        }
        if (isPost) {

        }
    }

    @Override
    public String doDefault() {
        return SUCCESS;
    }

    @Override
    protected String doExecute() throws Exception {
        AoQuote aoQuote = null;
        if (isPost) {
            quoteId = ServletActionContext.getRequest().getParameter("id");
            aoQuote = quoteDao.getById(quoteId);
            if (aoQuote != null) {
                aoQuote.setQuoteText(quoteText);
                aoQuote.setAuthorName(author);
                aoQuote.setCategory(Category.valueOf(category));
                aoQuote.save();
            } else {
                aoQuote = quoteDao.addQuote(new Quote(Category.valueOf(category), author, quoteText));
            }
            return getRedirect("room606.dq.QuoteOperation.jspa?id=" + aoQuote.getID());
        }
        return SUCCESS;
    }

    public String doRemove() throws Exception {
        if (hasAnyErrors()) {
            return ERROR;
        }
        quoteId = ServletActionContext.getRequest().getParameter("id");
        if (isEmpty(quoteId)) {
            return SUCCESS;
        }
        quoteDao.delete(quoteId);
        return getRedirect("room606.dq.ViewQuotes.jspa");
    }

    public String getQuoteText() {
        return quoteText;
    }

    public void setQuoteText(String quoteText) {
        this.quoteText = quoteText;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuoteId() {
        return quoteId;
    }

}
