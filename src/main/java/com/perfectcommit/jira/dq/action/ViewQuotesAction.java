package com.perfectcommit.jira.dq.action;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.perfectcommit.jira.dq.ao.dao.QuoteDao;
import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zugzug on 23.07.2017.
 */
public class ViewQuotesAction extends BaseAction {
    private final List<AoQuote> quotes = new LinkedList<>();

    public ViewQuotesAction(JiraAuthenticationContext jiraAuthenticationContext, QuoteDao quoteDao, PermissionManager permissionManager) {
        super(jiraAuthenticationContext, permissionManager);
        quotes.addAll(quoteDao.findAll());
    }

    protected void doValidation() {
        super.doValidation();
        if (getHasErrors()) {
            return;
        }
        if(!hasAdminAccess(getCurrentUser())) {
            addError("account", "Only admins have access to this page");
            return;
        }

    }

    public List<AoQuote> getQuotes() {
        return quotes;
    }
}
