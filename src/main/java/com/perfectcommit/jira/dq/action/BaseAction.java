package com.perfectcommit.jira.dq.action;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.perfectcommit.jira.dq.compat.JiraUserWrapper;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Created by zugzug on 21.07.2017.
 */
public class BaseAction extends JiraWebActionSupport {
    private final PermissionManager permissionManager;
    private JiraAuthenticationContext jiraAuthenticationContext;
    public BaseAction(JiraAuthenticationContext jiraAuthenticationContext, PermissionManager permissionManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
    }

    final String getJiraUser() {
        ApplicationUser user = jiraAuthenticationContext.getUser();
        if (user == null) {
            log.warn("User not logged in");
            return null;
        }
        return jiraAuthenticationContext.getUser().getUsername();
    }

    @Override
    protected void doValidation() {
        super.doValidation();
        String user = getJiraUser();
        if (user == null) {
            addError("account", "User is not logged in");
            return;
        }
    }


    public String beautify(String category) {
        return isEmpty(category) ? "" : category.replace('_', ' ');
    }

    public String convertToNoHTMLText(String content) {
        String cleanText = Jsoup.clean(content.replace("<br>", "\r\n").replace("\n\n", "").replace("\r\n\r\n", ""), Whitelist.relaxed());
        return cleanText;
    }

    public JiraUserWrapper getCurrentUser() {
        return new JiraUserWrapper(jiraAuthenticationContext.getUser());
    }

    public boolean hasAdminAccess(JiraUserWrapper user) {
        return user == null ? false : permissionManager.hasPermission(Permissions.ADMINISTER, user.getApplicationUser());
    }
}
