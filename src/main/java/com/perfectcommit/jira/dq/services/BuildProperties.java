package com.perfectcommit.jira.dq.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * Created by zugzug on 10.08.16.
 */
public class BuildProperties {

    private static final String PROPERTY_FILE = "build.properties";
    private static Logger log = Logger.getLogger(BuildProperties.class);

    private Properties prop = new Properties();

    public BuildProperties() {
        try {
            InputStream input = BuildProperties.class.getClassLoader().getResourceAsStream(PROPERTY_FILE);

            if (input == null) {
                log.error("Property file not find:" + PROPERTY_FILE);
                return;
            }
            prop.load(input);

        } catch (IOException ex) {
            log.error("Can't load plugin properties", ex);
        }

    }

    public String getPluginKey() {
        return prop.getProperty("plugin-key");
    }

    public String getPluginResourceKey(String resourceKey) {
        return String.format("%s:%s", getPluginKey(), resourceKey);
    }
}
