package com.perfectcommit.jira.dq.services;

import java.util.EnumSet;
import java.util.Set;
import javax.annotation.concurrent.GuardedBy;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.perfectcommit.jira.dq.ao.dao.QuoteDaoImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created by zugzug on 27.07.16.
 */
public class PluginEventsListener implements LifecycleAware, InitializingBean, DisposableBean {
    private final EventPublisher eventPublisher;
    private final BuildProperties buildProperties;

    private static Logger log = Logger.getLogger(PluginEventsListener.class);

    @GuardedBy("this")
    private final Set<LifecycleEvent> lifecycleEvents = EnumSet.noneOf(LifecycleEvent.class);
    private final QuoteDaoImpl quoteDao;

    static enum LifecycleEvent {
        PLUGIN_ENABLED,
        LIFECYCLE_AWARE_ON_START
    }


    public PluginEventsListener(EventPublisher eventPublisher, BuildProperties buildProperties, QuoteDaoImpl quoteDao) {
        this.eventPublisher = eventPublisher;
        this.buildProperties = buildProperties;
        this.quoteDao = quoteDao;
    }

    @EventListener
    public void onPluginEnableEvent(PluginEnabledEvent event) {
        if (buildProperties.getPluginKey().equals(event.getPlugin().getKey())) {
            onLifecycleEvent(LifecycleEvent.PLUGIN_ENABLED);
        }
    }

    @EventListener
    public void onPluginModuleDisabledEvent(PluginModuleDisabledEvent event) {
    }

    @EventListener
    public void onPluginDisable(PluginDisabledEvent event) {
    }

    @EventListener
    public void onPluginUninstalledEvent(PluginUninstalledEvent event) {
    }


    @Override
    public void onStart() {
        onLifecycleEvent(LifecycleEvent.LIFECYCLE_AWARE_ON_START);
    }

    /**
     * The latch which ensures all of the plugin/application lifecycle progress is completed before we call
     * {@code launch()}.
     */
    private void onLifecycleEvent(LifecycleEvent event) {
        if (isLifecycleReady(event)) {

        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    synchronized private boolean isLifecycleReady(LifecycleEvent event) {
        return lifecycleEvents.add(event) && lifecycleEvents.size() == LifecycleEvent.values().length;
    }
}
