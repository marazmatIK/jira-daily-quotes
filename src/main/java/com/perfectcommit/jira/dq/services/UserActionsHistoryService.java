package com.perfectcommit.jira.dq.services;

import com.perfectcommit.jira.dq.domain.Quote;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zugzug on 19.07.2017.
 */
public class UserActionsHistoryService {
    public static class UserActions {
        public UserActions(long newQuoteShownLastDate, Quote quote) {
            this.newQuoteShownLastDate = newQuoteShownLastDate;
            this.quote = quote;
        }

        private long newQuoteShownLastDate;
        private Quote quote;

        public Quote getQuote() {
            return quote;
        }

        public void setQuoteId(Quote quote) {
            this.quote = quote;
        }

        public long getNewQuoteShownLastDate() {
            return newQuoteShownLastDate;
        }

        public void setNewQuoteShownLastDate(long newQuoteShownLastDate) {
            this.newQuoteShownLastDate = newQuoteShownLastDate;
        }
    }

    private static final Map<String, UserActions> data = new ConcurrentHashMap<String, UserActions>();

    public UserActions getUserActions(String user) {
        return data.get(user);
    }

    public void updateNewQuoteShownInfo(String user, Quote quote, long date) {
        UserActions userActions = data.get(user);
        synchronized (this) {
            if (userActions == null) {
                userActions = new UserActions(date, quote);
                data.put(user, userActions);
            } else {
                userActions.newQuoteShownLastDate = date;
            }
        }
    }
}
