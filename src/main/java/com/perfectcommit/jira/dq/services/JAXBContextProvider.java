package com.perfectcommit.jira.dq.services;

import com.perfectcommit.jira.dq.domain.QuotesList;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.util.HashMap;
import java.util.Map;

public class JAXBContextProvider {
    private JAXBContext jaxbQuotesContext;
    private static final Map<String, JAXBContext> jaxbContextMap = new HashMap<String, JAXBContext>();
    private static final Logger log = Logger.getLogger(JAXBContextProvider.class);
    private static final JAXBContextProvider INSTANCE = new JAXBContextProvider();

    public static JAXBContextProvider getInstance() {
        return INSTANCE;
    }

    private JAXBContextProvider() {
        try {
            jaxbQuotesContext = JAXBContext.newInstance(QuotesList.class);
            jaxbContextMap.put(QuotesList.class.getName(), jaxbQuotesContext);
        } catch (JAXBException jaxbException) {
            log.error("Cannot get JAXBContext object for Quote!", jaxbException);
        }
    }

    public JAXBContext getJaxbContext(Class clazz) {
        return jaxbContextMap.get(clazz.getName());
    }
}