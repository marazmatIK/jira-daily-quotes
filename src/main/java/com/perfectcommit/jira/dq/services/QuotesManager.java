package com.perfectcommit.jira.dq.services;

import com.perfectcommit.jira.dq.ao.dao.QuoteDao;
import com.perfectcommit.jira.dq.ao.dao.UserConfigDao;
import com.perfectcommit.jira.dq.ao.dao.model.UserConfig;
import com.perfectcommit.jira.dq.domain.Category;
import com.perfectcommit.jira.dq.domain.Quote;
import com.perfectcommit.jira.dq.domain.QuotesList;
import org.apache.log4j.Logger;

import javax.xml.bind.*;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.perfectcommit.jira.dq.domain.Quote.toQuote;

/**
 * Created by nchernov on 21-Jul-16.
 */
public class QuotesManager {
    private static Logger log = Logger.getLogger(QuotesManager.class);
    private final UserActionsHistoryService userActionsHistoryService;
    private final QuoteDao quoteDao;
    private final UserConfigDao userConfigDao;
    private final long NEW_QUOTE_INTERVAL = 60 * 60 * 1000L;
    /**
     * For test purposes only!
     */
    int tickSize = 60;

    public QuotesManager(QuoteDao quoteDao, UserConfigDao userConfigDao, UserActionsHistoryService userActionsHistoryService) {
        this.quoteDao = quoteDao;
        this.userConfigDao = userConfigDao;
        this.userActionsHistoryService = userActionsHistoryService;
    }

    public List<Quote> loadQuotes() {
        return null;
    }

    public List<Quote> loadFromResources(String resourceName) throws JAXBException {

        JAXBContext jaxbContext = JAXBContextProvider.getInstance().getJaxbContext(QuotesList.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        QuotesList quotes = (QuotesList) jaxbUnmarshaller.unmarshal(QuotesManager.class.getResourceAsStream("/quotes/" + resourceName));
        return quotes == null ? Collections.<Quote>emptyList() : quotes.getQuotes();
    }

    public List<Quote> loadQuotes(Category category) {
        return null;
    }

    public List<Quote> loadQuotes(String author) {
        return null;
    }

    public Quote loadRandomQuote() {
        Random r = new Random();
        int count = quoteDao.quotesCount();
        int pos = r.nextInt(count);
        return toQuote(quoteDao.get(pos));
    }

    public Quote quoteForUser(String user) {
        Quote quote;
        UserActionsHistoryService.UserActions userActions = userActionsHistoryService.getUserActions(user);
        UserConfig userConfig = userConfigDao.getConfig(user);
        long interval = NEW_QUOTE_INTERVAL;
        if (userConfig!= null) {
            interval = userConfig.getQuotePeriod() * tickSize * 1000L;
        }
        interval = interval <= 0 ? NEW_QUOTE_INTERVAL : interval;

        if (userActions == null) {
            quote = loadRandomQuote();
            userActionsHistoryService.updateNewQuoteShownInfo(user, quote, System.currentTimeMillis());
        } else if (System.currentTimeMillis() >= (userActions.getNewQuoteShownLastDate() + interval)){
            quote = loadRandomQuote();
            userActionsHistoryService.updateNewQuoteShownInfo(user, quote, System.currentTimeMillis());
        } else {
            quote = userActions.getQuote();
        }
        log.debug("Returning quote (" + quote.getAuthorName() + ", " + quote.getText() == null ? "" : quote.getText().substring(0, quote.getText().length() / 2) + ") for user " + user);
        return  quote;
    }
}
