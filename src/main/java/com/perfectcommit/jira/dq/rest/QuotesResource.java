package com.perfectcommit.jira.dq.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.perfectcommit.jira.dq.services.QuotesManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by nchernov on 21-Jul-16.
 */
@Path("quote")
public class QuotesResource {
    private final QuotesManager quotesManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public QuotesResource(QuotesManager quotesManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.quotesManager = quotesManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response quote() {
        if (jiraAuthenticationContext.isLoggedInUser()) {
            return Response.ok(quotesManager.quoteForUser(jiraAuthenticationContext.getUser().getUsername())).build();
        } else {
            return Response.status(401).build();
        }
    }
}
