package com.perfectcommit.jira.dq.compat;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

import java.security.Principal;

/**
 * Created by zugzug on 23.07.2017.
 */
public class JiraUserWrapper {

    private Principal delegate;

    public JiraUserWrapper(Principal delegate) {
        if (delegate == null) {
            throw new NullPointerException("Delegate object is null");
        }
        this.delegate = delegate;
    }

    public String getKey() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getKey();
        } else {
            return null;
        }
    }

    public String getUsername() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getUsername();
        } else {
            return null;
        }
    }

    public String getName() {
        return delegate.getName();
    }

    public long getDirectoryId() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getDirectoryId();
        } else {
            return ((User) delegate).getDirectoryId();
        }
    }

    public boolean isActive() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).isActive();
        } else {
            return ((User) delegate).isActive();
        }
    }

    public String getEmailAddress() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getEmailAddress();
        } else {
            return ((User) delegate).getEmailAddress();
        }
    }

    public String getDisplayName() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getDisplayName();
        } else {
            return ((User) delegate).getDisplayName();
        }
    }

    public User getDirectoryUser() {
        if (delegate instanceof ApplicationUser) {
            return ((ApplicationUser) delegate).getDirectoryUser();
        } else {
            return (User) delegate;
        }
    }

    public ApplicationUser getApplicationUser() {
        if (delegate instanceof ApplicationUser) {
            return (ApplicationUser) delegate;
        } else {
            return ApplicationUsers.from((User) delegate);
        }
    }

    public boolean equals(Object user) {
        return delegate.equals(user);
    }

    public int hashCode() {
        return delegate.hashCode();
    }
}
