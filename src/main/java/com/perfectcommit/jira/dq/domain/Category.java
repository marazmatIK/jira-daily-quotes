package com.perfectcommit.jira.dq.domain;

/**
 * Created by nchernov on 21-Jul-16.
 */
public enum Category {
    SOFTWARE, QA, MANAGEMENT, BUSINESS, SELF_IMPROVEMENT, SCIENCE, HUMOR, INSPIRATION, DESIGN
}
