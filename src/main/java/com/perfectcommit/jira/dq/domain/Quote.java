package com.perfectcommit.jira.dq.domain;

import com.perfectcommit.jira.dq.ao.dao.model.AoQuote;
import org.codehaus.jackson.map.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by nchernov on 21-Jul-16.
 */
@XmlRootElement(name = "quote")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Quote {
    private Category category;

    private String authorName;
    private String authorPosition;
    private String text;
    private String authorUrl;
    private List<String> keyWords = new ArrayList<>();

    public static Quote toQuote(AoQuote aoQuote) {
        return new Quote(aoQuote.getCategory(), aoQuote.getAuthorName(), aoQuote.getAuthorPosition(),
                aoQuote.getQuoteText(),
                aoQuote.getKeyWords() == null ? Collections.<String>emptyList() : Arrays.asList(aoQuote.getKeyWords().split("[,]", -1)));
    }

    public Quote() {
    }

    public Quote(Category category, String authorName, String authorPosition, String text, List<String> keyWords) {
        this.category = category;
        this.authorName = authorName;
        this.authorPosition = authorPosition;
        this.text = text;
        this.keyWords = keyWords;
    }

    public Quote(Category category, String authorName, String authorPosition, String text) {
        this.category = category;
        this.authorName = authorName;
        this.authorPosition = authorPosition;
        this.text = text;
    }

    public Quote(Category category, String authorName, String text) {
        this.category = category;
        this.authorName = authorName;
        this.text = text;
    }

    @XmlElement(name = "category")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @XmlElement(name = "authorName")
    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @XmlElement(name = "authorPosition")
    public String getAuthorPosition() {
        return authorPosition;
    }

    public void setAuthorPosition(String authorPosition) {
        this.authorPosition = authorPosition;
    }

    @XmlElement(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlElement(name = "keyWord")
    @XmlElementWrapper(name = "keyWords")
    public List<String> getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(List<String> keyWords) {
        this.keyWords = keyWords;
    }

    @XmlElement(name = "authorUrl")
    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public static void main(String[] args) throws IOException, JAXBException {
        Quote quote = new Quote(Category.SOFTWARE,
                "Linus Torvalds",
                "Talk is cheap. Show me the code.");
        quote.setAuthorUrl("");

        Quote quote2 = new Quote(Category.SOFTWARE,
                "Linus Torvalds",
                "Talk is cheap. Show me the code.");
        quote2.setAuthorUrl("");

        QuotesList quotes = new QuotesList();
        quotes.setQuotes(Arrays.asList(quote, quote2));

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(quotes);
        System.out.println(json);

        JAXBContext jaxbContext = JAXBContext.newInstance(QuotesList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        jaxbMarshaller.marshal(quotes, byteArrayOutputStream);
        String marshalled = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
        System.out.println(marshalled);
    }
}
