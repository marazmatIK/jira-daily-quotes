package com.perfectcommit.jira.dq.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by nchernov on 26-Jul-16.
 */
@XmlRootElement(name = "quotesList")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class QuotesList {
    private List<Quote> quotes;

    @XmlElement(name = "quote")
    @XmlElementWrapper(name = "quotes")
    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }
}
