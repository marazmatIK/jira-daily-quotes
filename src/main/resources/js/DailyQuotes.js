function DailyQuotes(gadget) {

    var $ = AJS.$;
    var DATE_TIME_FORMAT = 'YYYY-MM-DD hh:mm A Z';
    var self = this;
    var tbody;
    var config;
    var gadget = gadget;

    this.initGadget = function(options) {
        var that = this;
        this.exclude = options ? options.exclude : '';
        $('head').append('<meta name = "decorator" content="admin" />');
        $.ajax({
            url: (gadget ? '' : AJS.contextPath()) + '/rest/dailyquotes/1.0/quote',
                type: 'GET',
                success: function(response) {
                    self.fillView(response);
                },
                error: function(response) {
                    console.error(response);
                    self.fillView({text: 'Kindness is the language which the deaf can hear and the blind can see.', authorName: 'Mark Twain', category: 'HUMOR'});
                }
        });
    };

    this.beautify = function(category) {
        return category.replace('_', ' ');
    }

    this.fillView = function(data) {
        var words = [];
        if(data) {
            var quoteMessage = perfectcommit.dc.Templates.quoteMessage({text: data.text, author: data.authorName, category: this.beautify(data.category), baseUrl: AJS.contextPath()});
            gadget.getView().html(quoteMessage);
        }
    };

};